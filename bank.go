package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/rs/xid"
)

type bankData struct {
	Position    int `json:"id"`
	LoanAccount struct {
		ID                         string `json:"id"`
		AccountHolderKey           string `json:"accountHolderKey"`
		AccountHolderType          string `json:"accountHolderType"`
		CreationDate               string `json:"creationDate"`
		LastModifiedDate           string `json:"lastModifiedDate"`
		AccountState               string `json:"accountState"`
		ProductTypeKey             string `json:"productTypeKey"`
		LoanName                   string `json:"loanName"`
		LoanAmount                 string `json:"loanAmount"`
		PrincipalDue               string `json:"principalDue"`
		CreditScore                int    `json:"creditScore"`
		PrincipalPaid              string `json:"principalPaid"`
		PrincipalBalance           string `json:"principalBalance"`
		InterestDue                string `json:"interestDue"`
		InterestPaid               string `json:"interestPaid"`
		InterestBalance            string `json:"interestBalance"`
		FeesDue                    string `json:"feesDue"`
		FeesPaid                   string `json:"feesPaid"`
		FeesBalance                string `json:"feesBalance"`
		PenaltyDue                 string `json:"penaltyDue"`
		PenaltyPaid                string `json:"penaltyPaid"`
		PenaltyBalance             string `json:"penaltyBalance"`
		ScheduleDueDatesMethod     string `json:"scheduleDueDatesMethod"`
		RepaymentPeriodCount       int    `json:"repaymentPeriodCount"`
		RepaymentPeriodUnit        string `json:"repaymentPeriodUnit"`
		RepaymentInstallments      int    `json:"repaymentInstallments"`
		GracePeriod                int    `json:"gracePeriod"`
		GracePeriodType            string `json:"gracePeriodType"`
		InterestRate               string `json:"interestRate"`
		InterestChargeFrequency    string `json:"interestChargeFrequency"`
		InterestCalculationMethod  string `json:"interestCalculationMethod"`
		RepaymentScheduleMethod    string `json:"repaymentScheduleMethod"`
		PaymentMethod              string `json:"paymentMethod"`
		InterestApplicationMethod  string `json:"interestApplicationMethod"`
		Notes                      string `json:"notes"`
		PrincipalRepaymentInterval int    `json:"principalRepaymentInterval"`
		InterestRateSource         string `json:"interestRateSource"`
		InterestAdjustment         string `json:"interestAdjustment"`
		AccruedInterest            string `json:"accruedInterest"`
		ArrearsTolerancePeriod     int    `json:"arrearsTolerancePeriod"`
		Guarantees                 []struct {
			GuarantorKey      string `json:"guarantorKey"`
			Amount            string `json:"amount"`
			GuarantorType     string `json:"guarantorType"`
			CustomFieldValues []struct {
				CustomFieldID string `json:"CustomFieldID"`
				Value         string `json:"value"`
			} `json:"customFieldValues"`
		} `json:"guarantees"`
		Funds []struct {
			GuarantorKey       string `json:"guarantorKey"`
			SavingsAccountKey  string `json:"savingsAccountKey"`
			Amount             string `json:"amount"`
			InterestCommission string `json:"interestCommission"`
		} `json:"funds"`
	} `json:"loanAccount"`
	DisbursementDetails struct {
		ExpectedDisbursementDate string `json:"expectedDisbursementDate"`
		FirstRepaymentDate       string `json:"firstRepaymentDate"`
		TransactionDetails       struct {
			TransactionChannel struct {
				ID         string `json:"id"`
				EncodedKey string `json:"encodedKey"`
			} `json:"transactionChannel"`
			ReceiptNumber string `json:"receiptNumber"`
			BankNumber    string `json:"bankNumber"`
		} `json:"transactionDetails"`
		Fees []struct {
			Fee struct {
				EncodedKey string `json:"encodedKey"`
			} `json:"fee"`
			Amount string `json:"amount,omitempty"`
		} `json:"fees"`
	} `json:"disbursementDetails"`
	CustomInformation []struct {
		Value         string `json:"value"`
		CustomFieldID string `json:"customFieldID"`
	} `json:"customInformation"`
}

func main() {
	startTime := time.Now()
	fmt.Println("Started at: ", startTime)

	file, err := os.Create("sorted_bank_file.txt")

	if err != nil {
		fmt.Println("MayDAY!: ", err)
	}
	count := 10000
	

	jsn, err := ioutil.ReadFile("new_loan.txt")

	if err != nil {
		fmt.Println(err)
	}


	for count != 0 {

		newUID := genXid()

		fileData := bankData{}

		err = json.Unmarshal(jsn, &fileData)
		if err != nil {
			log.Fatal("error while parsing: ", err)
		}

		rand.Seed(time.Now().UnixNano())
		userCreditScore := rand.Intn(10)

		position := 1
		fileData.Position = position
		fileData.LoanAccount.CreditScore = userCreditScore
		fileData.LoanAccount.AccountHolderKey = newUID
		finalData, err := json.MarshalIndent(fileData, "", "\t")
		if err != nil {
			fmt.Println("An error occured: ", err)
		}
		str := string(finalData)
		// strArray := append(finalFile, str)
		fmt.Fprintf(file, str)
		fmt.Fprintf(file, ",")

		// fmt.Println(strArray)
		position++
		count--
		// fmt.Println("success: ", str)
	}
	// toWrite := strings.Join(finalFile, ",")
	// fmt.Fprintf(file, (toWrite))

	elapsed := time.Since(startTime)

	fmt.Println("success!!! Completed in ", elapsed)

	// fmt.Printf("%v\n", finalData)

}

func genXid() string {
	id := xid.New()
	return id.String()
}
